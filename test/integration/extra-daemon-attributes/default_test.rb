# frozen_string_literal: true

# Inspec test for recipe docker-ce-cookbook::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe directory('/etc/docker') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0755' }
end

describe file('/etc/docker/daemon.json') do
  it { should exist }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
  its('mode') { should cmp '0600' }
  its('content') do
    should match(/"exec-opts":s?\[\s?+"native.cgroupdriver=systemd"\s?+\]/m)
  end
  its('content') do
    should match(%r{"registry-mirrors":s?\[\s?+"http://localhost:5000"\s?+\]}m)
  end
  its('content') { should match(/"live-restore":\s?true/) }
  its('content') { should match(/"log-driver":\s?"journald"/) }
  its('content') { should match(/"mtu":\s?1450/) }
end

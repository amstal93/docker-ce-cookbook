# frozen_string_literal: true

name              'docker-ce'
maintainer        'Pharmony SA'
maintainer_email  'dev@pharmony.lu'
license           'MIT'
description       'Installs Docker CE'
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           '0.15.0'
chef_version      '>= 13.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://gitlab.com/pharmony/docker-ce-cookbook/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://gitlab.com/pharmony/docker-ce-cookbook'

depends 'apt'

%w[debian ubuntu].map do |platform|
  supports platform
end

# Testing the Docker CE cookbook

This cookbook uses the Kitchen CI in order to run the tests.
All the configuration can be found in the `.kitchen.yml` (Dokken driver) and `.kitchen.docker.yml` (Docker driver) files.

## First time

You need to install the dependencies in order to get kitchen drivers availables.

From this repo's folder:

```
$ bundle
```

## Usage

### Integration tests

You can list the Kitchens :

```
$ bundle exec kitchen list
Instance          Driver  Provisioner  Verifier  Transport  Last Action    Last Error
default-debian-8  Dokken  Dokken       Inspec    Dokken     <Not Created>  <None>
default-debian-9  Dokken  Dokken       Inspec    Dokken     <Not Created>  <None>
...
```

To run the tests, simply run `bundle exec kitchen test` :

```
$ bundle exec kitchen test
-----> Starting Kitchen (v1.24.0)
-----> Cleaning up any prior instances of <default-debian-9>
-----> Destroying <default-debian-9>...
...
-----> Verifying <default-debian-9>...
       Loaded tests from {:path=>".Users.zedtux.Developments.docker-ce-cookbook.test.integration.default"}

Profile: tests from {:path=>"/Users/zedtux/Developments/docker-ce-cookbook/test/integration/default"} (tests from {:path=>".Users.zedtux.Developments.docker-ce-cookbook.test.integration.default"})
Version: (not specified)
Target:  docker://22a28cca31bb89f5b5051d7f4adb0084288113e18b181c7bd2d60eaacf14a407

  System Package apt-transport-https
     ✔  should be installed
  System Package ca-certificates
     ✔  should be installed
  System Package curl
     ✔  should be installed
  System Package gnupg2
     ✔  should be installed
  System Package software-properties-common
     ✔  should be installed
  Command: `apt-key adv --list-public-keys`
     ✔  stdout should match /9DC858229FC7DD38854AE2D88D81803C0EBFCD88/
  Apt Repository https://download.docker.com/linux/debian
     ✔  should exist
  System Package docker-ce
     ✔  should be installed
  System Package docker-ce-cli
     ✔  should be installed
  System Package containerd.io
     ✔  should be installed

Test Summary: 10 successful, 0 failures, 0 skipped
       Finished verifying <default-debian-9> (0m5.39s).
-----> Destroying <default-debian-9>...
       Deleting kitchen sandbox at /Users/zedtux/.dokken/kitchen_sandbox/358b85956f-default-debian-9
       Deleting verifier sandbox at /Users/zedtux/.dokken/verifier_sandbox/358b85956f-default-debian-9
       Finished destroying <default-debian-9> (0m11.68s).
       Finished testing <default-debian-9> (2m28.62s).
-----> Kitchen is finished. (2m32.58s)
```

### Testing with the Docker driver

All you need to do is to prefix the above commands with `KITCHEN_YAML=.kitchen.docker.yml` :

```
$ KITCHEN_YAML=.kitchen.docker.yml bundle exec kitchen list
Instance          Driver  Provisioner  Verifier  Transport  Last Action    Last Error
default-debian-8  Docker  ChefZero     Inspec    Ssh        <Not Created>  <None>
default-debian-9  Docker  ChefZero     Inspec    Ssh        <Not Created>  <None>
...
```

And the same for running tests :

```
$ KITCHEN_YAML=.kitchen.docker.yml bundle exec kitchen test
...
```

### Testing Docker upgrades

Kitchen as accepted the RFC084 which would have allowed this test to be performed, but unfortunately the RFC is not yet implemented so we made a small script to do it at `bin/test-docker-upgrade`.

You can simply run it as the following:

```
$ UPGRADE_DOCKER_TO_VERSION='18.09' UPGRADE_DOCKER_WITH_PLATFORM='debian-9' ./bin/test-docker-upgrade
```
_Please note that this will use the dokken driver_
